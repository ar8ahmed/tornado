package edu.purdue.cs.tornado.evaluator;

public enum ArithmeticOperator {
	//Arithmetic operators should be evaluatoed in postfix or prex notation
	PLUS,
	MINUS,
	TIMES,
	DIVIDE,
	POWER,
}
