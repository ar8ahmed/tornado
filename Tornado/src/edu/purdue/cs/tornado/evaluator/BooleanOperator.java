package edu.purdue.cs.tornado.evaluator;

public enum BooleanOperator {
	LESS_THAN,
	LESS_EQUAL,
	GREATER_THAN,
	GREATER_EQUAL,
	EQUAL,
	NOT_EQUAL
}
