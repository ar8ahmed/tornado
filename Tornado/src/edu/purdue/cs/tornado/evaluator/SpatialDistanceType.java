package edu.purdue.cs.tornado.evaluator;

public enum SpatialDistanceType {
	HAMMING,// Hamming from point
	EUCLIDEAN, // EUCLIDEAN from point
	MIN_DIST, // EUCLIDEAN from rectangle
	MAX_DIST, // EUCLIDEAN from rectangle
	ROAD_NETWORK //TODO
}
