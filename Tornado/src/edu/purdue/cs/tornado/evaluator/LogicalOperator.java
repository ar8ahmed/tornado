package edu.purdue.cs.tornado.evaluator;

public enum LogicalOperator {
	AND,
	OR,
	NOT
}
