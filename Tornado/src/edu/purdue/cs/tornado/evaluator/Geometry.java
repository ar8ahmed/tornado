package edu.purdue.cs.tornado.evaluator;

public enum Geometry {
	POINT,
	RECTANGLE,
	POLYGON
}
